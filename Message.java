import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.nio.*;

enum MessageType {
	req_key,resp_key,resp_req_key,term;
}
public class Message implements Serializable{
	AtomicInteger timestamp;
	MessageType messageType;
	Node node_info;
	public Message(AtomicInteger timestamp, MessageType messageType, Node node_info)
	{
		this.timestamp = timestamp;								//message timestamp
		this.messageType = messageType;							//message type
		this.node_info = node_info;								//Info of sender node
	}
	
	public ByteBuffer toByteBuffer() throws Exception
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		oos.flush();
		
		ByteBuffer buf = ByteBuffer.allocateDirect(bos.size());
		buf.put(bos.toByteArray());
		
		oos.close();
		bos.close();

		// Buffer needs to be flipped after writing
		// Buffer flip should happen only once		
		buf.flip();
		return buf;
	}

	// Retrieve Message from ByteBuffer received from SCTP
	public static Message fromByteBuffer(ByteBuffer buf) throws Exception
	{
		// Buffer needs to be flipped before reading
		// Buffer flip should happen only once
		buf.flip();
		byte[] data = new byte[buf.limit()];
		buf.get(data);
		buf.clear();

		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInputStream ois = new ObjectInputStream(bis);
		Message msg = (Message) ois.readObject();

		bis.close();
		ois.close();

		return msg;
	}
}
