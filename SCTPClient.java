//Client Implementation for SCTP Communication
import java.io.*;
import java.net.*;
import java.util.*;
import com.sun.nio.sctp.*;
import java.nio.ByteBuffer;

public class SCTPClient implements Runnable
{
	public static final int _SIZE = 1024;
	private Node recvr_node;
	private Message message;
	
	public SCTPClient(Node recvr_node, Message message)
	{
		this.recvr_node=recvr_node;
		this.message=message;
	}
	
	public void run()
	{
		String response;
		try
		{
			Thread.sleep(1000);
			SocketAddress addr = new InetSocketAddress(recvr_node.name, recvr_node.port);		//Create address from host name & port id
			SctpChannel sc = SctpChannel.open();
			sc.connect(addr);										//Establish a connection, Can I get a connection? Can I get, can I get a conn
			System.out.println("Sending to: "+addr);
			MessageInfo messageInfo = MessageInfo.createOutgoing(null,0);
													
			sc.send(message.toByteBuffer(), messageInfo);
		}
		catch(Exception e){}
	}
}		
