//Server Implementation for SCTP Communication
import java.io.*;
import java.net.*;
import java.util.*;
import java.text.*;
import com.sun.nio.sctp.*;
import java.nio.ByteBuffer;
import java.nio.file.*;

//TODO: Sending responses to PRQueue messages

public class SCTPServer extends Main implements Runnable
{
	public static final int _SIZE=1024;
	public static int msgs;
	public void run()
	{
		//Start listening on specified port
		int port = node_map.get(node_id).port;
		InetSocketAddress addr = new InetSocketAddress(port);		//Create socket address from port info
		try{
			SctpServerChannel ssc = SctpServerChannel.open();			//Open SCTP Server channel
			ssc.bind(addr);
			System.out.println("Server running on : "+addr);
			Thread.sleep(10000);
			
			pr_queue=getPQ();
			
			while(true)
			{
				SctpChannel sc=ssc.accept();				//Accept incoming connection
				Thread.sleep(1000);
				
				ByteBuffer buf = ByteBuffer.allocateDirect(_SIZE);
				MessageInfo msg_info = sc.receive(buf, null, null);
				curr_timestamp.incrementAndGet();
				Message msg = Message.fromByteBuffer(buf);
				int sender_id;
				sender_id=msg.node_info.id;				//Node ID of the sender
				String sender_name=msg.node_info.name;
				
				if(msg.messageType == MessageType.req_key)
				{
					System.out.println("Key requested by: "+sender_name);
					if(busy)					//Currently executing CS so add request to the Queue
					{
						pr_queue.add(msg);
					}
					
					else if(requested_cs)				//Has it's own CS Request pending
					{
						if(msg.timestamp.get()>curr_timestamp.get())			//Whose own request was early then add new request to Queue
						{
							pr_queue.add(msg);
						}
						else if(msg.timestamp.get()==curr_timestamp.get())		//Same timestamp then check node IDs
						{
							if(node_id>sender_id)
							{
								node_map.get(sender_id).n_key=false;
								SendMsg(msg.node_info, MessageType.resp_key);
							}
							else
							{
								pr_queue.add(msg);
							}
						}
						else								//New request was before, so send response immediately
						{
							node_map.get(sender_id).n_key=false;
							SendMsg(msg.node_info, MessageType.resp_key);
						}
					}
					else									//Node is idle so and has no request so send response
					{
						System.out.println("Key received from: "+sender_name);
						node_map.get(sender_id).n_key=false;
						SendMsg(msg.node_info, MessageType.resp_key);
					}
				}
				else										//Receiving a Response key
				{
					node_map.get(sender_id).n_key=true;					//Key is now known
					node_map.get(sender_id).requested=false;
				}
				
			}
		}catch (Exception e){}
	}
	
	public synchronized void request_keys()								//Request all keys to enter CS
	{
		System.out.println("Requesting all keys\n");
		Node n;
		msgs=0;
		for(int nid:node_map.keySet())
		{
			n=node_map.get(nid);
			if(!n.n_key && node_id!=n.id && node_map.get(n.id).requested!=true)			//Key is not known and is not requested earlier
			{
				SendMsg(n, MessageType.req_key);
				node_map.get(n.id).requested=true;						//Well it is Requested NOW, Happy?
				msgs+=1;
			}
		}
	}
	
	public boolean all_keys()										//Does this node has all keys known?
	{
		boolean hasall=true;
		Node n;
		for(int nid:node_map.keySet())
		{
			n=node_map.get(nid);
			if(n.id!=node_id)
			{
				if(!n.n_key)
				{
					hasall=false;								//No you don't
					return hasall;
				}
			}
		}
		return hasall;											//To key or not to key
	}
	
	public synchronized void SendMsg(Node node, MessageType mtype)					//Sending messages to node
	{
		if(node.id!=node_id)
		{
			if (mtype == MessageType.req_key)
			{
				System.out.println("Requesting key from: "+node.name);
				node_map.get(node.id).requested = true;
			}
			else
			{
				System.out.println("Sending key to:"+node.name);
				node_map.get(node.id).n_key = false;
			}
														//look thrugh this later
			Message message = new Message(curr_timestamp, mtype, node_map.get(node_id));
			SCTPClient client = new SCTPClient(node, message);
			new Thread(client).start();
		}
	}
	
	public synchronized void NotBusyRespondNow(PriorityQueue<Message> prq, MessageType mtype)		//You were busy earlier but you have messages, hurry n respond
	{
		int size = prq.size();
		Thread[] clients = new Thread[size];								//Threads to handle responses 
		
		int i=0;
		curr_timestamp.incrementAndGet();
		while(prq.size()>0)
		{
			Message m = prq.poll();								//pop a message, it's always gonna be a request message?
			if(m!=null)
			{
				Node node = m.node_info;							//Who sent this message?
				if(node_id!=node.id)
				{
					node_map.get(node.id).n_key = false;					//Don't have that node's key anymore
					
					Message message = new Message(curr_timestamp, mtype, node_map.get(node_id));
					
					SCTPClient client=new SCTPClient(node, message);			//Send response to these nodes
					clients[i] = new Thread(client);
					clients[i].start();
					i++;
				}
			}
		}
	}
}
	
	
						
