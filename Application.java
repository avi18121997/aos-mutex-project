//Responsible for generating and executing Critical Section requests
//TODO: How to calculate Message and Time complexity
import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.*;
import java.nio.file.*;

public class Application implements Runnable
{
	public static int total_nodes;							//Total nodes
	//static Random rand;
	public static int inter_req_delay;							//Inter Request Delay
	public static int exec_time;								//Critical Section exec time
	public static int n_requests;								//Number of requests to be generated
	private Main mutex = new Main();
	
	public Application(int total_nodes, int inter_req_delay, int exec_time, int n_requests)
	{
		this.total_nodes=total_nodes;
		this.inter_req_delay=inter_req_delay;
		this.exec_time=exec_time;
		this.n_requests=n_requests;
	}
	public void cs_execute()
	{
		long start_time = System.currentTimeMillis();
		long curr_time = System.currentTimeMillis();
		long e_time = exec_time;
		long end_time = start_time+e_time;
		
		while(curr_time<end_time)
		{
			System.out.println("Executing Critical Section\n");
			curr_time = System.currentTimeMillis();
			mutex.curr_timestamp.incrementAndGet();
		}
	}
	public void run()
	{
		long time_complexity[] = new long[n_requests];
		long summ=0;
		try
		{
			String xy = "node"+mutex.node_id+".log";
			BufferedWriter b = new BufferedWriter(new FileWriter(xy));
			
			for(int i=0; i<n_requests; i++)
			{
				long t1 = System.currentTimeMillis();				//Time at which request was generated
				mutex.cs_enter();
				String start = mutex.curr_timestamp.toString();
				cs_execute();
				String end = mutex.curr_timestamp.toString();
				long t2 = System.currentTimeMillis();				//Time at which execution was completed
				mutex.cs_leave();
				long t3 = t2-t1;
				time_complexity[i]=t3;						//Time of request completion
				Thread.sleep(inter_req_delay);				//Sleep for inter_req_delay
				summ+=t3;
				b.write(start+" "+end);
				
			}
			b.close();
		}catch(Exception e){}
		double response_time = summ/n_requests;					//avg response time
		double avg_message_complexity = mutex.message_complexity/n_requests;		//average message complexity
		
		if(mutex.node_id==0)
		{
			try
			{
				File file = new File("Performance.txt");
				if(!file.exists())
				{
					file.createNewFile();
				}
				BufferedWriter out = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
				out.write(total_nodes+" "+n_requests+" "+avg_message_complexity+" "+response_time);		//Performance of node 0 on this run
				out.close();
			}catch(IOException e){}
		}
	}
}
	
	
