//Verification
//Test the correctness of implementation
import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;
import java.nio.file.*;

public class Verification
{
	public static void main(String[] args)
	{
		SortedMap<Integer, Integer> timestamps = new TreeMap<Integer, Integer>();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("config.txt"));
			String x = br.readLine();
			String params[] = x.split(" ");
			int n = Integer.parseInt(params[0]);
			
		
			for(int i=0; i<n; i++)
			{
				String y = "node"+i+".log";
				List<String> allLines = Files.readAllLines(Paths.get(y));
				allLines.removeAll(Arrays.asList("", null));
				for(int j=0; j<allLines.size(); j++)
				{
					String a[] = (allLines.get(j)).split(":");
					int st = Integer.parseInt(a[0]);
					int ed = Integer.parseInt(a[1]);
					if(timestamps.containsKey(st))
					{
						System.out.println("Violation of MutEx protocol found\n");
						//System.exit(0);
					}
					else
					{
						timestamps.put(st,ed);
					}
				}
			}
		}catch(Exception e){}
		
		Set set = timestamps.keySet();
		int arr[] = new int[set.size()];
		for(int i=0; i<set.size(); i++)
		{
			int a=arr[i+1];								//Start time of a node's CS
			int b=arr[i];									//Start time of previous node's CS
			int c=timestamps.get(b);							//End time of previous node's CS
			if(a<=c)									//End time of prev node cannot be greater than start time of next node?
			{
				System.out.println("Violation of MutEx protocol found\n");
				//System.exit(0);
			}
		}
		
		System.out.println("No violations were found\n");	
		
	}
}
		
			
