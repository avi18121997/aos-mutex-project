import java.io.*;

public class Node implements Serializable{
	
	public int id;										//ID of node
	public String name;									//Name of machine
	public int port;									//Port
	public boolean requested;								//Is this noe's key requested?
	public boolean terminated;
	public boolean n_key;									//Doth you haveth thy key?...Is this node's key known?

	
	public Node(int id, String name, int port, boolean n_key, boolean requested)
	{
		this.id = id;
		this.name = name;
		this.port = port;
		this.n_key = n_key;
		this.requested = requested;
		this.terminated = false;
	}
	
	public Node()
	{
		this.id = 0;
		this.name = "";
		this.port = 0;	
		this.n_key = false;
		this.requested = false;
		this.terminated = false;
	}
}

