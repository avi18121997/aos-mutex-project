import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.*;
import java.nio.file.*;
//TODO: Read config file and create a hashmap, log file, Message complexity

public class Main
{
	public static int node_id;								//ID of this node
	public static int total_nodes;							//Total nodes
	//static Random rand;
	public static int inter_req_delay;							//Inter Request Delay
	public static int exec_time;								//Critical Section exec time
	public static int n_requests;								//Number of requests to be generated
	//public static Boolean terminated=false;
	
	public static HashMap<Integer, Node> node_map;					//A map of all nodes with ID:Node
	public static int waiting=0;
	
	public static boolean busy=false;							//Busy if executing i.e in CS
	public static boolean requested_cs=false;						//CS request is pending
	public static PriorityQueue<Message> pr_queue = getPQ();				//Priority Queue to handle requests
	public static AtomicInteger curr_timestamp = new AtomicInteger(0);			//Current timestamp of this node
	public static int message_complexity=0;
	static SCTPServer serv = new SCTPServer();
	
	public static PriorityQueue<Message> getPQ()						//Initialize a PQ, priority given to lower timestamp
	{
		PriorityQueue<Message> queue = new PriorityQueue<Message>(11, new Comparator<Message>(){
		public int compare(Message m1, Message m2)
		{
			AtomicInteger i1 = m1.timestamp;
			AtomicInteger i2 = m2.timestamp;
			if(i1.get()>=i2.get())
				return 1;
			else
				return -1;
		}
		}
		);
	return queue;
	}
	
	public void cs_enter()									//Called when a node generates a CS request
	{
		System.out.println("Requesting Critical Section");
		requested_cs=true;
		curr_timestamp.incrementAndGet();
		serv.request_keys();								//Request all keys
		message_complexity+=serv.msgs;
		while(!serv.all_keys())							//Wait till you have all keys
		{
			try
			{
				System.out.println("Missing keys...");
				curr_timestamp.incrementAndGet();
				Thread.sleep(3000);
			}catch(Exception e){}
		}
		busy=true;
		//Log the start time
		return;
	}
	
	public void cs_leave()
	{
		//Log the end time
		System.out.println("Leaving Critical Section");
		busy=false;
		requested_cs=false;								//dja it can't generate a request while in CS
		pr_queue=getPQ();
												//One CS request complete
		serv.NotBusyRespondNow(pr_queue, MessageType.resp_key);			//Send pending responses
		
		
		return;
	}
	
	public static void main(String[] args) throws IOException
	{
		node_id=Integer.parseInt(args[0]);
		node_map=new HashMap<Integer, Node>();
		//read config file
		try
		{
			List<String> allLines = Files.readAllLines(Paths.get("config.txt"));
			allLines.removeAll(Arrays.asList("", null));
			String p = allLines.get(0);
			String[] parameters = p.split(" ");
			total_nodes = Integer.parseInt(parameters[0]);
			inter_req_delay = Integer.parseInt(parameters[1]);
			exec_time = Integer.parseInt(parameters[2]);
			n_requests = Integer.parseInt(parameters[3]);
			
			System.out.println("Parameters:\n"+"Total Nodes: "+total_nodes+" Inter Request Delay: "+inter_req_delay+" Execution Time: "+exec_time+" Total number of requests: "+n_requests);
			int i=0;
			while(i<total_nodes)
			{
				String x = allLines.get(i+1);
				System.out.println(x);
				String[] infos = x.split(" ");
				int _id = Integer.parseInt(infos[0]);
				String _name = infos[1];
				int _port = Integer.parseInt(infos[2]);
				//System.out.println(_id+" "+_name+" "+_port);
				i++;
				System.out.println(i);
				Node n = new Node(_id, _name, _port, false, false);
				node_map.put(_id, n);
			}
			
		}catch(Exception e){}
				
		//start server
		Thread t = new Thread(serv);
		t.start();
		Application App_Client = new Application(total_nodes, inter_req_delay, exec_time, n_requests);
		Thread s = new Thread(App_Client);
		s.start();
	}
}	
	
